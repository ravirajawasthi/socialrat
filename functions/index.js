const express = require("express");
const functions = require("firebase-functions");
const FBauth = require("./utils/FbAuth");
const cors = require("cors");
const { db } = require("./utils/admin");

const app = express();

const {
  getAllScreams,
  postOneScream,
  getScream,
  commentOnScream,
  likeScream,
  unlikeScream,
  deleteScream,
} = require("./handlers/screams");
const {
  signUp,
  login,
  uploadImage,
  updateUserInfo,
  getAuthenticatedUser,
  getUserDetails,
  markNotificationsRead,
} = require("./handlers/users");

app.use(cors({ origin: true }));

app.post("/signup", signUp);
app.post("/login", login);
app.post("/user/image", FBauth, uploadImage);
app.post("/user", FBauth, updateUserInfo);
app.get("/user", FBauth, getAuthenticatedUser);
app.get("/user/:handle", getUserDetails);
app.post("/notifications", FBauth, markNotificationsRead);

app.get("/screams", getAllScreams);
app.post("/scream", FBauth, postOneScream);
app.get("/scream/:screamId", getScream);
app.post("/scream/:screamId/comment", FBauth, commentOnScream);
app.post("/scream/:screamId/like", FBauth, likeScream);
app.post("/scream/:screamId/unlike", FBauth, unlikeScream);
app.post("/scream/:screamId/delete", FBauth, deleteScream);

exports.api = functions.https.onRequest(app);

//Cloud functions based on trigger like

exports.createNotificationOnLike = functions.firestore
  .document("/likes/{id}")
  .onCreate((snapshot) => {
    const createdLike = snapshot.data();
    db.doc(`/screams/${createdLike.screamId}`) //CreateLike cannot be undefined because this function is run when a notification is created
      .get()
      .then((doc) => {
        const screamLiked = doc.data();
        if (screamLiked.userHandle !== createdLike.userHandle) {
          const newNotification = {
            recipient: screamLiked.userHandle,
            sender: createdLike.userHandle,
            read: false,
            screamId: createdLike.screamId,
            type: "like",
            createdAt: new Date().toISOString(),
          };
          db.collection("notifications")
            .add(newNotification)
            .then((newNotification) => {
              return true;
            })
            .catch((err) => console.error(err));
        } else {
          return false;
        }
      })
      .catch((err) => console.error(err));
  });

exports.deleteNotificationOnUnlike = functions.firestore
  .document("/likes/{id}")
  .onDelete((snapshot) => {
    const deletedLike = snapshot.data();
    db.collection("notifications")
      .where("type", "==", "like")
      .where("screamId", "==", deletedLike.screamId)
      .where("sender", "==", deletedLike.userHandle)
      .limit(1)
      .get()
      .then((snapshot) => {
        return snapshot.docs[0].id;
      })
      .then((notificationIdToBeDeleted) => {
        db.doc(`/notifications/${notificationIdToBeDeleted}`)
          .delete()
          .then(() => {
            return true;
          })
          .catch((err) => console.error(err));
      })
      .catch((err) => console.error(err));
  });

//Cloud functions based on trigger comment

exports.createNotificationOnComment = functions.firestore
  .document("/comments/{id}")
  .onCreate((snapshot) => {
    const createdComment = snapshot.data();
    db.doc(`/screams/${createdLike.screamId}`)
      .get()
      .then((doc) => {
        const screamCommented = doc.data();
        if (createdComment.userHandle !== screamCommented.userHandle) {
          const newNotification = {
            recipient: screamLiked.userHandle,
            sender: createdComment.userHandle,
            read: false,
            screamId: createdLike.screamId,
            type: "comment",
            createdAt: new Date().toISOString(),
          };
          db.collection("notifications")
            .add(newNotification)
            .then((newNotification) => {
              return true;
            })
            .catch((err) => console.error(err));
        } else {
          return false;
        }
      })
      .catch((err) => console.error(err));
  });

exports.createNotificationOnDeleteComment = functions.firestore
  .document("/comments/{id}")
  .onDelete((snapshot) => {
    const deletedComment = snapshot.data();
    db.collection("notifications")
      .where("type", "==", "comment")
      .where("screamId", "==", deletedComment.screamId)
      .where("sender", "==", deletedComment.userHandle)
      .limit(1)
      .get()
      .then((snapshot) => {
        return snapshot.docs[0].id;
      })
      .then((notificationIdToBeDeleted) => {
        db.doc(`/notifications/${notificationIdToBeDeleted}`)
          .delete()
          .then(() => {
            return true;
          })
          .catch((err) => console.error(err));
      })
      .catch((err) => console.error(err));
  });

exports.onUserImageChange = functions.firestore
  //if a user changes his image then we have to change imageUrl everywhere
  //on all scream he has made
  //on all comments he mode on screams
  .document("/users/{imageUrl}")
  .onUpdate((snapshot) => {
    const newImageUrl = snapshot.after.data().imageUrl;
    let batch = db.batch();
    db.collection("screams")
      .where("userHandle", "==", snapshot.after.data().handle)
      .get()
      .then((screams) => {
        screams.forEach((scream) => {
          batch.update(db.doc(`/screams/${scream.id}`), {
            userImage: newImageUrl,
          });
        });
      })
      .then(() => {
        return db
          .collection("comments")
          .where("userHandle", "==", snapshot.after.data().handle)
          .get()
          .then((comments) => {
            comments.forEach((comment) => {
              batch.update(db.doc(`/comments/${comment.id}`), {
                userImage: newImageUrl,
              });
            });
          })
          .catch((err) => {
            console.error(err);
            return false;
          });
      })
      .then(() => {
        batch
          .commit()
          .then(() => {
            return true;
          })
          .catch((err) => {
            console.error(err);
            return false;
          });
      })
      .catch((err) => {
        console.error(err);
        return false;
      });
  });

exports.onScreamDelete = functions.firestore
  .document("/screams/{screamId}")
  .onDelete((snapshot, context) => {
    const screamId = context.params.screamId;
    const batch = db.batch();
    return db
      .collection("comments")
      .where("screamId", "==", screamId)
      .get()
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(db.doc(`/comments/${doc.id}`));
        });
        return db.collection("likes").where("screamId", "==", screamId).get();
      })
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(db.doc(`/likes/${doc.id}`));
        });
        return db
          .collection("notifications")
          .where("screamId", "==", screamId)
          .get();
      })
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(db.doc(`/notifications/${doc.id}`));
        });
        return batch.commit();
      })
      .catch((err) => console.error(err));
  });
