const emptyError = "Must not be empty";

const isEmail = (email) => {
  const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (email.match(regEx)) return true;
  else return false;
};

const isEmpty = (string) => {
  if (!string || string.trim() === "") return true;
  else return false;
};

exports.validateSignupData = (data) => {
  let errors = {};

  if (isEmpty(data.email)) {
    errors.email = emptyError;
  } else if (!isEmail(data.email)) {
    errors.email = "Must be a valid email address";
  }

  if (isEmpty(data.password)) errors.password = emptyError;
  if (data.password !== data.confirmPassword) {
    errors.confirmPassword = "Passwords must match";
    {
      !errors.password && (errors.password = "Passwords must match");
    }
  }

  if (isEmpty(data.handle)) errors.handle = emptyError;

  return {
    errors,
    valid: Object.keys(errors).length === 0 ? true : false,
  };
};

exports.validateLoginData = (data) => {
  let errors = {};

  if (isEmpty(data.email)) errors.email = emptyError;
  if (isEmpty(data.password)) errors.password = emptyError;

  return {
    valid: Object.keys(errors).length > 0 ? false : true,
    errors,
  };
};

exports.checkUserUpdateInfo = (data) => {
  let errors = {};
  if (!isEmpty(data.bio.trim())) errors.bio = data.bio;

  if (!isEmpty(data.location.trim())) errors.location = data.location;

  if (!isEmpty(data.website.trim())) {
    if (data.website.substring(0, 4) !== "http") {
      errors.website = "https://" + data.website;
    } else errors.website = data.website;
  }
  return {
    data,
    errors,
  };
};

exports.validateNewScream = (body) => {
  const errors = {};
  if (isEmpty(body.body)) errors.scream = emptyError;
  return { errors, valid: Object.keys(errors).length === 0 ? true : false };
};
