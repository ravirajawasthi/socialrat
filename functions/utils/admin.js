const admin = require("firebase-admin");
const serviceAccount = require("../keys/serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  storageBucket: "gs://socialrat-772eb.appspot.com",
});

const db = admin.firestore();

module.exports = { db, admin };
