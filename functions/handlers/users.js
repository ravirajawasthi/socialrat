const { db, admin } = require("../utils/admin");
const firebase = require("firebase");
const BusBoy = require("busboy");
const path = require("path");
const os = require("os");
const fs = require("fs");
const firebaseConfig = require("../keys/firebaseConfig.json");

firebase.initializeApp(firebaseConfig);

const {
  validateSignupData,
  validateLoginData,
  checkUserUpdateInfo,
} = require("../utils/validators");

exports.signUp = (req, res) => {
  const newUser = {
    email: req.body.email,
    password: req.body.password,
    confirmPassword: req.body.confirmPassword,
    handle: req.body.handle,
  };

  const { valid, errors } = validateSignupData(newUser);

  if (!valid) return res.status(400).json(errors);
  let token, userId;
  db.doc(`/users/${newUser.handle}`)
    .get()
    .then((doc) => {
      if (doc.exists) {
        return res.status(400).json({ handle: "this handle is already taken" });
      } else {
        return firebase
          .auth()
          .createUserWithEmailAndPassword(newUser.email, newUser.password)
          .then((data) => {
            userId = data.user.uid;
            return data.user.getIdToken();
          })
          .then((idToken) => {
            token = idToken;
            const userCredentials = {
              handle: newUser.handle,
              email: newUser.email,
              createdAt: new Date().toISOString(),
              imageUrl:
                "https://firebasestorage.googleapis.com/v0/b/socialrat-772eb.appspot.com/o/no-img.png?alt=media",
              userId,
            };
            return db
              .doc(`/users/${userCredentials.handle}`)
              .set(userCredentials);
          })
          .then(() => {
            return res.status(201).json({ token });
          })
          .catch((err) => {
            console.error(err);
            if (err.code === "auth/email-already-in-use") {
              return res
                .status(400)
                .json({ email: "email is already in used" });
            } else {
              return res.status(500).json({ general: "Something went wrong" });
            }
          });
      }
    });
};

exports.login = (req, res) => {
  const user = {
    email: req.body.email,
    password: req.body.password,
  };

  const { valid, errors } = validateLoginData(user);

  if (!valid) return res.status(400).json(errors);
  firebase
    .auth()
    .signInWithEmailAndPassword(user.email, user.password)
    .then((data) => {
      return data.user.getIdToken();
    })
    .then((token) => {
      return res.json({ token });
    })
    .catch((err) => {
      res.status(401).json({ general: "Invalid Credentials, Try Again!." });
    });
};

exports.uploadImage = (req, res) => {
  let imageToBeUploaded = {};
  let imageFileName;
  const busboy = new BusBoy({ headers: req.headers });

  busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
    //Check image type
    if (mimetype !== "image/jpeg" && mimetype !== "image/png") {
      return res.status(400).json({ error: "Wrong file type submitted" });
    }
    //Generate Random name for image
    const imageExtension = filename.split(".")[filename.split(".").length - 1];
    imageFileName = `${req.user.handle}.${imageExtension}`;
    const filepath = path.join(os.tmpdir(), imageFileName);
    imageToBeUploaded = { filepath, mimetype };
    file.pipe(fs.createWriteStream(filepath));
  });
  busboy.on("finish", () => {
    //Update user image
    admin
      .storage()
      .bucket()
      .upload(imageToBeUploaded.filepath, {
        resumable: false,
        metadata: {
          metadata: {
            contentType: imageToBeUploaded.mimetype,
          },
        },
      })
      .then(() => {
        const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${firebaseConfig.storageBucket}/o/${imageFileName}?alt=media`;
        return db.doc(`/users/${req.user.handle}`).update({ imageUrl });
      })
      .then(() => {
        return res.json({ message: "Image uploaded successfully" });
      })
      .catch((err) => {
        console.error(err);
        return res.status(500).json({ error: "Something went wrong!" });
      });
  });
  busboy.end(req.rawBody);
};
exports.updateUserInfo = (req, res) => {
  const userData = req.body;
  db.doc(`/users/${req.user.handle}`)
    .update({ ...userData })
    .then(() => {
      return res.json({
        message: `Details for ${req.user.handle} updated successfully`,
      });
    })
    .catch((err) => {
      console.error(err);
      res.json({ error: err.code });
    });
};

exports.getAuthenticatedUser = (req, res) => {
  let likes = [];
  let credentials;
  db.doc(`/users/${req.user.handle}`)
    .get()
    .then((docRef) => {
      credentials = docRef.data();
      db.collection("likes")
        .where("userHandle", "==", req.user.handle)
        .get()
        .then((likeRef) => {
          likeRef.forEach((like) => likes.push(like.data()));
          return db
            .collection("notifications")
            .where("recipient", "==", req.user.handle)
            .orderBy("createdAt")
            .limit(10)
            .get();
        })
        .then((docs) => {
          const notifications = [];
          if (!docs.empty) {
            docs.forEach((notification) =>
              notifications.push(notification.data())
            );
          }
          return notifications;
        })
        .then((notifications) => {
          return res.json({ credentials, likes, notifications });
        });
    })
    .catch((err) => {
      console.error(err);
      res.json({ error: err.code });
    });
};

exports.getUserDetails = (req, res) => {
  const userHandle = req.params.handle;
  const userData = {};
  db.doc(`/users/${userHandle}`)
    .get()
    .then((user) => {
      if (user.exists) {
        const fetchedUser = user.data();
        delete fetchedUser.email;
        userData.user = fetchedUser;
        return fetchedUser;
      } else {
        return res.status(404).json({ error: "User does not exist" });
      }
    })
    .then((user) => {
      return db
        .collection("screams")
        .where("userHandle", "==", userHandle)
        .orderBy("createdAt")
        .limit(10)
        .get();
    })
    .then((snapshot) => {
      userData.screams = [];
      if (!snapshot.empty) {
        snapshot.forEach((scream) => {
          userData.screams.push({ ...scream.data(), screamId: scream.id });
        });
      }
      return res.json(userData);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: err.code });
    });
};

exports.markNotificationsRead = (req, res) => {
  let batch = db.batch();
  const notificationsIds = req.body;
  let ref;
  notificationsIds.forEach((notificationsId) => {
    ref = db.doc(`/notifications/${notificationsId}`);
    batch.update(ref, { read: true });
  });
  batch
    .commit()
    .then(() => {
      return res.json({ message: "Notifications mark read" });
    })
    .catch((err) => {
      console.error(err);
      return res.json({ error: err.code });
    });
};
