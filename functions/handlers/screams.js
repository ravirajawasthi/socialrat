const { db, admin } = require("../utils/admin");
const { validateNewScream } = require("../utils/validators");

exports.getAllScreams = (req, res) => {
  db.collection("screams")
    .orderBy("createdAt", "desc")
    .get()
    .then((data) => {
      let screams = [];
      data.forEach((doc) => {
        screams.push({ screamId: doc.id, ...doc.data() });
      });
      res.json(screams);
    })
    .catch((err) => console.error(err));
};

exports.postOneScream = (req, res) => {
  if (req.method !== "POST") {
    return res.status(400).json({ error: "Method not allowed" });
  }
  const validator = validateNewScream({ body: req.body.body });
  if (validator.valid) {
    const newScream = {
      body: req.body.body,
      userHandle: req.user.handle,
      createdAt: new Date().toISOString(),
      userImage: req.user.imageUrl,
      likeCount: 0,
      commentCount: 0,
    };
    db.collection("screams")
      .add(newScream)
      .then((createdScream) => {
        const responseScream = newScream;
        responseScream.screamId = createdScream.id;
        return res.json({
          ...responseScream,
        });
      });
  } else {
    return res.status(402).json({ ...validator.errors });
  }
};

exports.getScream = (req, res) => {
  let screamData = {};
  db.doc(`/screams/${req.params.screamId}`)
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return res.status(404).json({ error: "Scream not found" });
      }
      screamData = doc.data();
      screamData.screamId = doc.id;
      // In database there was a whitespace somewhere in the screamId,
      // I spent 7 hours trying to figure it out
      return db
        .collection("comments")
        .orderBy("createdAt", "desc")
        .where("screamId", "==", req.params.screamId)
        .get();
    })
    .then((data) => {
      screamData.comments = [];
      data.forEach((doc) => {
        screamData.comments.push(doc.data());
      });
      return res.json(screamData);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: err.code });
    });
};

exports.commentOnScream = (req, res) => {
  const commentObj = {
    body: req.body.body,
    createdAt: new Date().toISOString(),
    screamId: req.params.screamId,
    userHandle: req.user.handle,
    userImage: req.user.imageUrl,
  };
  const screamDoc = db.doc(`/screams/${req.params.screamId}`);
  screamDoc
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return res.status(404).json({ error: "Scream does not exist" });
      } else if (commentObj.body.trim() === "") {
        res.status(404).json({ comment: "Comment must not be empty" });
      }
      return screamDoc;
    })
    .then(() => {
      db.collection("comments")
        .add(commentObj)
        .then((commentRef) => {
          screamDoc
            .update({
              commentCount: admin.firestore.FieldValue.increment(1),
            })
            .then(() => {
              commentRef
                .get()
                .then((commentSnapshot) => {
                  return res.json(commentSnapshot.data());
                })
                .catch((err) => {
                  res.status(500).json({ comment: "Internal Server error" });
                });
            })
            .catch((err) => {
              console.err(err);
              res.json({ error: err.code });
            });
        })
        .catch((err) => {
          console.err(err);
          res.json({ error: err.code });
        });
    });
};

exports.likeScream = (req, res) => {
  //Check if there exists a document with userHandle and screamId
  //else like the scream by creating a like document and incrementing the likeCount
  const likeDocument = db
    .collection("likes")
    .where("userHandle", "==", req.user.handle)
    .where("screamId", "==", req.params.screamId)
    .limit(1);
  const screamDocument = db.doc(`/screams/${req.params.screamId}`);
  let screamData;
  screamDocument
    .get()
    .then((doc) => {
      if (doc.exists) {
        screamData = doc.data();
        screamData.screamId = doc.id;
        return likeDocument.get();
      } else {
        return res.status(404).json({ error: "Scream not found" });
      }
    })
    .then((data) => {
      if (data.empty) {
        return db
          .collection("likes")
          .add({
            screamId: req.params.screamId,
            userHandle: req.user.handle,
          })
          .then(() => {
            screamData.likeCount++;
            return screamDocument.update({ likeCount: screamData.likeCount });
          })
          .then(() => {
            return res.json(screamData);
          });
      } else {
        return res.status(400).json({ error: "Scream already liked" });
      }
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: err.code });
    });
};

exports.unlikeScream = (req, res) => {
  const screamId = req.params.screamId;
  let likeObj;
  const screamDoc = db.doc(`/screams/${screamId}`);
  let responseObj;
  screamDoc
    .get()
    .then((scream) => {
      if (scream.exists) {
        responseObj = scream.data();
        responseObj.screamId = screamId;
        return db
          .collection("likes")
          .where("userHandle", "==", req.user.handle)
          .where("screamId", "==", screamId);
      } else {
        return res.status(404).json({ error: "Scream does not exist" });
      }
    })
    .then((doc) => {
      doc.get().then((likesSnapshot) => {
        if (!likesSnapshot.empty) {
          const likeIdToBeDeleted = likesSnapshot.docs[0].id;
          db.doc(`likes/${likeIdToBeDeleted}`)
            .delete()
            .then(() => {
              screamDoc
                .update({
                  likeCount: admin.firestore.FieldValue.increment(-1),
                })
                .then((result) => {
                  responseObj.likeCount--;
                  res.json({ ...responseObj });
                });
            });
        } else {
          res.status(401).json({ error: "Invalid operation" });
        }
      });
    })
    .catch((err) => {
      console.error(error);
      res.status(500).json({ error: err.code });
    });
};

exports.deleteScream = (req, res) => {
  const screamId = req.params.screamId;
  const screamRef = db.doc(`/screams/${screamId}`);
  screamRef.get().then((data) => {
    if (data.exists) {
      const scream = data.data();
      if (scream.userHandle === req.user.handle) {
        screamRef.delete().then(() => {
          return res.json({ message: "scream deleted" });
        });
      } else {
        return res.status(403).json({ error: "Unauthorized" });
      }
    } else {
      return res.status(404).json({ error: "scream does not exist" });
    }
  });
};
