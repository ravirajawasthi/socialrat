//React Imports
import React from "react";
import { Switch, Route } from "react-router-dom";
import Axios from "axios";
import jwtDecode from "jwt-decode";
import { logoutUser, getUserData } from "./redux/actions/userActions";

//MUI imports
import Navbar from "./components/Navbar";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Signup from "./pages/Signup";
import AuthRoute from "./components/AuthRoute";
import User from "./pages/User";
import makeStyles from "@material-ui/core/styles/makeStyles";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";


//Redux Imports
import { STOP_USER_LOADING } from "./redux/type";
import { connect } from "react-redux";
import store from "./redux/store";

const theme = createMuiTheme({
  palette: {
    primary: {
      light: "#33c9dc",
      main: "#00bcd4",
      dark: "#008394",
      contrastText: "#fff"
    },
    secondary: {
      light: "#ff6333",
      main: "#ff3d00",
      dark: "#b22a00",
      contrastText: "#fff"
    }
  }
});

const styles = {
  container: {
    margin: "64px auto 0 auto",
    maxWidth: "1366px"
  }
};

function App({ getUserData, logoutUser }) {
  const token = localStorage.getItem("FbIdToken");
  if (token) {
    const decodedToken = jwtDecode(token);
    if (decodedToken.exp * 1000 < Date.now()) {
      logoutUser();
    } else {
      Axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
      getUserData();
    }
  } else {
    store.dispatch({ type: STOP_USER_LOADING });
  }
  const classes = makeStyles(styles)();
  return (
    <MuiThemeProvider theme={theme}>
      <Navbar />
      <div className={classes.container}>
        <Switch>
          <Route
            exact
            path="/"
            render={routeProps => <Home {...routeProps} />}
          />
          <AuthRoute exact path="/login" Component={Login} />
          <AuthRoute exact path="/signup" Component={Signup} />
          <Route
            exact
            path="/user/:userHandle"
            render={routeProps => <User {...routeProps} />}
          />
        </Switch>
      </div>
    </MuiThemeProvider>
  );
}

const mapActionsToProps = {
  getUserData,
  logoutUser
};

export default connect(null, mapActionsToProps)(App);
