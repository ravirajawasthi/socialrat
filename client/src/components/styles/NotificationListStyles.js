const styles = (theme) => ({
  MenuItem: {
    color: theme.palette.text.primary,
    "&:hover": {
      "& svg": {
        color: theme.palette.common.white,
      },
      color: theme.palette.common.white,
      backgroundColor: theme.palette.primary.main,
    },
  },
  Menu: {
    border: "1px solid #d3d4d5",
  },
  IconContainer: {
    marginRight: "5px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  Icon: {
    color: theme.palette.primary.main,
  },
  IconButton: {
    color: `${theme.palette.common.white} `,
  },
});

export default styles;
