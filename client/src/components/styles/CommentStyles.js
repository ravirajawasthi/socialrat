const styles = () => ({
    commentContainer: {
        width: "100%",
        display: "flex",
        alignItems: "center",
        margin: "10px 0"
    },
    imageContainer: {
        width: "100px",
        height: "100px"
    },
    image: {
        width: "100%",
        height: "100%",
        objectFit: "cover",
        borderRadius: "50%",
    },
    commentContent: {
        height: "100px",
        marginLeft: "20px",
        width: "70%",        
        alignItems: "flex-start",
        display: "inline-block",
    }
})

export default styles