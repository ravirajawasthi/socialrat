const styles = {
    content: {
        width: "100%",
    },
    userImage: {
        width: "20%",
        display: "inline",
        objectFit: "cover"
    },
    screamDetails: {
        width: "60%",
        display: "inline",
        padding: 10,
    },
    cardContainer: {
        display: 'flex',
        marginBottom: 20,
        maxHeight: "149px",
        boxSizing: "border-box"
    },
    userHandle: {
        display: "flex",
        justifyContent: "space-between"
    },
    
}

export default styles