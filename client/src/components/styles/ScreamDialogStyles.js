const styles = {
  mainContainer: {
    display: "flex",
    flexDirection: "column",
    
  },
  screamContainer: {
    width: "100%",
    display: "flex",
    marginBottom: "20px"
  },
  screamContent: {
    display: "flex",
    flexDirection: "column",
    width: "70%",
  },
  imageContainer: {
    width: "250px",
    height: "250px",
    marginRight: "50px"
  },
  userImage: {
    width: "100%",
    height: "100%",
    objectFit: "cover",
    borderRadius: "50%",
    boxShadow: "10px 10px 19px -8px rgba(0,0,0,0.39)"
  },
  loadingIcon: {
    margin: "20px auto"
  }
};
export default styles;
