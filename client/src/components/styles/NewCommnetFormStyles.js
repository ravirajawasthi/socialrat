const styles = () => ({
  root: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyItems: "center",
    alignItems: "center",
    margin: "10px 0"
  },
  commentTextField: {
      width: "90%",
      margin: "30px auto"
  }
});

export default styles