const styles = theme => ({
    profileContainer:{
        padding: theme.spacing(1),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    Image: {
        width: "60%",
        borderRadius: "50%",
        marginBottom: `${theme.spacing(1)}px`
    },
    profileAttribute: {
        padding: `${theme.spacing(0.66)}px 0`,
        display: "flex",
        alignItems: "center",
        "& svg":{
            margin: "0 5px"
        }
    },
    actionBtn: {
        margin: theme.spacing(0.4),
        display: "inline"
    },
    ImageContainer: {
        position: 'relative',
        display: 'flex',
        justifyContent: "center"
    },
    ChangeImage: {
        position: "absolute",
        right: "5%",
        bottom: 0,
    },
    
})
export default styles