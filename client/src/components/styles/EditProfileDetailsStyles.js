const styles = theme => ({
    profileActions: {
        padding: `${theme.spacing(0.66)}px 0`,
        display: "flex",
        justifyContent: "space-between"
    }
})

export default styles