const styles = {
  Navbar: {
    maxHeight: "64px"
  },
  Link: {
    "& a": {
      textDecoration: "none",
      color: "white"
    }
  },
  navContainer: {
    display: "flex",
    margin: "auto"
  }
};

export default styles;
