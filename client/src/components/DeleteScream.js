import React, { useState } from "react";

import { connect } from "react-redux";
import { deleteScream } from "../redux/actions/dataActions";

import Avatar from "@material-ui/core/Avatar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DeleteIcon from "@material-ui/icons/Delete";
import CloseIcon from "@material-ui/icons/Close";
import IconButton from "@material-ui/core/IconButton";

import withStyles from "@material-ui/core/styles/withStyles"

const styles = () => ({
  deleteIcon: {
    padding: 0
  }
})

function DeleteScream({ deleteScream, screamId, classes }) {
  const [open, setOpen] = useState(false);
  const openDialog = () => setOpen(true);
  const closeDialog = () => setOpen(false);
  return (
    <>
      <IconButton className={classes.deleteIcon} color="secondary" onClick={openDialog}>
        <DeleteIcon />
      </IconButton>
      <Dialog
        onClose={closeDialog}
        aria-labelledby="Delete Scream confirmation"
        open={open}
      >
        <DialogTitle>Are you sure?</DialogTitle>
        <List>
          <ListItem autoFocus button onClick={() => deleteScream(screamId)}>
            <ListItemAvatar>
              <Avatar style={{ backgroundColor: "#ed7777" }}>
                <DeleteIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={"Delete"} />
          </ListItem>
          <ListItem button onClick={closeDialog}>
            <ListItemAvatar>
              <Avatar style={{ backgroundColor: "#24991d" }}>
                <CloseIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary="Cancel" />
          </ListItem>
        </List>
      </Dialog>
    </>
  );
}

export default withStyles(styles)(connect(null, { deleteScream })(DeleteScream))
