import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

function AuthRoute({ isAuthenticated, Component, path, exact }) {
  if (isAuthenticated) return <Redirect to="/" />;
  else
    return (
      <Route
        path={path}
        exact={exact}
        render={(routeProps) => <Component {...routeProps} />}
      />
    );
}

const mapStateToProps = (state) => ({
  isAuthenticated: state.user.authenticated,
});

export default connect(mapStateToProps, null)(AuthRoute);

// null denotes this component does not use any actions
