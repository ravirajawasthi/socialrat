import React, { useState } from "react";
import styles from "./styles/EditProfileDetailsStyles";
import withStyles from "@material-ui/core/styles/withStyles";

import { connect } from "react-redux";
import { logoutUser, updateUserInfo } from "../redux/actions/userActions";

import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Create from "@material-ui/icons/Create";
import LogoutIcon from "@material-ui/icons/PowerSettingsNew";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

function EditProfileDetails({
  classes,
  logoutUser,
  updateUserInfo,
  credentials
}) {
  const [open, setOpen] = useState(false);
  const [bio, setBio] = useState(credentials.bio ? credentials.bio : "");
  const [location, setLocation] = useState(
    credentials.location ? credentials.location : ""
  );
  const [website, setWebsite] = useState(
    credentials.website ? credentials.website : ""
  );

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = () => {
    updateUserInfo({ bio, location, website });
    handleClose();
  };

  return (
    <div className={classes.profileActions}>
      <Tooltip placement="top" title="Logout">
        <IconButton color="primary" onClick={logoutUser}>
          <LogoutIcon />
        </IconButton>
      </Tooltip>
      <Tooltip placement="top" title="Edit Details">
        <IconButton color="primary" onClick={handleClickOpen}>
          <Create />
        </IconButton>
      </Tooltip>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="Edit profile details"
        fullWidth
      >
        <DialogTitle>Change Profile Details</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            multiline
            row={3}
            label="Bio"
            type="text"
            value={bio}
            onChange={e => setBio(e.target.value)}
            fullWidth
          />
          <TextField
            label="Location"
            type="text"
            value={location}
            onChange={e => setLocation(e.target.value)}
            fullWidth
          />
          <TextField
            label="Website"
            type="text"
            value={website}
            onChange={e => setWebsite(e.target.value)}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

const mapStateToProps = state => ({
  credentials: state.user.credentials
});

export default connect(mapStateToProps, { logoutUser, updateUserInfo })(
  withStyles(styles)(EditProfileDetails)
);
