import React from "react";

import dayjs from "dayjs";

import { connect } from "react-redux";

import { Link } from "react-router-dom";

import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from "./styles/ProfileStyles";
import MuiLink from "@material-ui/core/Link";
import CalendarToday from "@material-ui/icons/CalendarToday";
import LocationOn from "@material-ui/icons/LocationOn";
import Home from "@material-ui/icons/Home";

function StaticProfile({
  classes,
  data: {
    loading,
    user: { user },
  },
}) {
  const profileMarkup = (
    <>
      <div className={classes.ImageContainer}>
        <img className={classes.Image} src={user.imageUrl} alt="profile" />
      </div>

      <Typography
        component={Link}
        to={`/user/${user.handle}`}
        variant="h5"
        color="primary"
      >{`@${user.handle}`}</Typography>

      {user.bio && <Typography variant="subtitle1">{user.bio}</Typography>}
      {user.createdAt && (
        <div className={classes.profileAttribute}>
          <CalendarToday />
          {dayjs(user.createdAt).format("MMM YYYY")}
        </div>
      )}
      {user.location && (
        <div className={classes.profileAttribute}>
          <LocationOn />
          {user.location}
        </div>
      )}
      {user.website && (
        <MuiLink className={classes.profileAttribute}>
          <Home />
          {user.website}
        </MuiLink>
      )}
    </>
  );

  return <Paper className={classes.profileContainer}>{profileMarkup}</Paper>;
}

const mapStateToProps = (state) => ({
  data: state.data,
  UI: state.UI,
});

export default withStyles(styles)(connect(mapStateToProps)(StaticProfile));
