import React from "react";

import EditUserDetails from "./EditProfileDetails";

import dayjs from "dayjs";

import { uploadImage } from "../redux/actions/userActions";
import { connect } from "react-redux";

import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from "./styles/ProfileStyles";
import MuiLink from "@material-ui/core/Link";
import CalendarToday from "@material-ui/icons/CalendarToday";
import LocationOn from "@material-ui/icons/LocationOn";
import Home from "@material-ui/icons/Home";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import Tooltip from "@material-ui/core/Tooltip";

import { Link } from "react-router-dom";

function Profile({
  classes,
  user: {
    loading,
    credentials: { handle, imageUrl, bio, location, createdAt, website },
    authenticated,
  },
  uploadImage,
  logoutUser,
}) {
  const SelectFileInput = () => {
    const fileInput = document.getElementById("imageUploadInput");
    fileInput.click();
  };

  const handleImageUpload = (event) => {
    const imageForm = new FormData();
    if (event.target.files[0]) {
      imageForm.append(
        "image",
        event.target.files[0],
        event.target.files[0].name
      );
      uploadImage(imageForm);
    }
  };

  const loadingMarkup = (
    <>
      <CircularProgress />
      <p>Loading...</p>
    </>
  );

  const notLoggedInMarkup = (
    <>
      <Typography variant="h5">Join our exciting platform!</Typography>

      <Button
        className={classes.actionBtn}
        color="primary"
        variant="contained"
        component={Link}
        to="/login"
      >
        Login
      </Button>
      <Button
        className={classes.actionBtn}
        color="secondary"
        variant="contained"
        component={Link}
        to="/signup"
      >
        Create Account
      </Button>
    </>
  );

  const profileMarkup = (
    <>
      <div className={classes.ImageContainer}>
        <img className={classes.Image} src={imageUrl} alt="profile" />
        <Tooltip title="Change Image" placement="top-end">
          <IconButton
            className={classes.ChangeImage}
            color="primary"
            onClick={SelectFileInput}
          >
            <PhotoCamera />
          </IconButton>
        </Tooltip>
      </div>
      <input
        type="file"
        id="imageUploadInput"
        accept="image/*"
        hidden={true}
        onChange={handleImageUpload}
      />
      <Typography
        component={Link}
        to={`/user/${handle}`}
        variant="h5"
        color="primary"
      >{`@${handle}`}</Typography>
      {bio && <Typography variant="subtitle1">{bio}</Typography>}
      {createdAt && (
        <div className={classes.profileAttribute}>
          <CalendarToday />
          {dayjs(createdAt).format("MMM YYYY")}
        </div>
      )}
      {website && (
        <MuiLink className={classes.profileAttribute}>
          <Home />
          {website}
        </MuiLink>
      )}
      {location && (
        <div className={classes.profileAttribute}>
          <LocationOn />
          {location}
        </div>
      )}
      <EditUserDetails />
    </>
  );

  return (
    <Paper className={classes.profileContainer}>
      {loading
        ? loadingMarkup
        : authenticated
        ? profileMarkup
        : notLoggedInMarkup}
    </Paper>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
  UI: state.UI,
});

const mapActionsToProps = {
  uploadImage,
};

export default withStyles(styles)(
  connect(mapStateToProps, mapActionsToProps)(Profile)
);
