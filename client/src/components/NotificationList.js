//React
import React, { useState } from "react";
import styles from "./styles/NotificationListStyles";
//MUI
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Like from "@material-ui/icons/ThumbUpAlt";
import Comment from "@material-ui/icons/Sms";
import NotificationIcon from "@material-ui/icons/Notifications";
import { withStyles } from "@material-ui/core";
//Redux
import { connect } from "react-redux";

function NotificationList({ classes, notifications }) {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const notificationMarkup =
    notifications.length === 0 ? (
      <h1>NO Notifications</h1>
    ) : (
      <>
        {notifications.map((notification) => (
          <MenuItem className={classes.MenuItem}>
            {/* Corrent Icon */}
            <div className={classes.IconContainer}>
              {notification.type === "like" ? (
                <Like className={classes.Icon} />
              ) : (
                <Comment className={classes.Icon} />
              )}
            </div>
            {/* Notification content */}
            <ListItemText>
              {notification.type === "like"
                ? `${notification.sender} liked your scream`
                : `${notification.sender} commented on your scream`}
            </ListItemText>
          </MenuItem>
        ))}
      </>
    );
  return (
    <>
      <Tooltip title={"Notifications"}>
        <IconButton
          className={classes.IconButton}
          aria-haspopup="true"
          variant="contained"
          onClick={handleClick}
        >
          <NotificationIcon />
        </IconButton>
      </Tooltip>

      <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        className={classes.Menu}
        id="notification-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {notificationMarkup}
      </Menu>
    </>
  );
}

const mapStateToProps = (state) => ({
  notifications: state.user.notifications,
});

export default connect(
  mapStateToProps,
  null
)(withStyles(styles)(NotificationList));
