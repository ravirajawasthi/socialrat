import React from "react";
import styles from "./styles/ScreamStyles";
import DeleteScream from "./DeleteScream";
import ScreamDialog from "./ScreamDialog";

import { connect } from "react-redux";
import { likeScream, unlikeScream } from "../redux/actions/dataActions";

import dayjs from "dayjs";

import relativeTime from "dayjs/plugin/relativeTime";

import { Link } from "react-router-dom";

import Card from "@material-ui/core/Card";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import { CardContent } from "@material-ui/core";

import LikeIcon from "@material-ui/icons/Favorite";
import UnlikeIcon from "@material-ui/icons/FavoriteBorder";
import IconButton from "@material-ui/core/IconButton";
import CommentIcon from "@material-ui/icons/Comment";

function Scream({ scream, classes, likeScream, unlikeScream, user }) {
  dayjs.extend(relativeTime);
  const hasUserLikedScream = () => {
    if (
      user.likes &&
      user.likes.find((like) => like.screamId === scream.screamId)
    )
      return true;
    else return false;
  };
  const LikeButton = (
    <IconButton color="primary">
      {!user.authenticated ? (
        <Tooltip title="like" placement="top">
          <Link to="/signup">
            <UnlikeIcon />
          </Link>
        </Tooltip>
      ) : hasUserLikedScream() ? (
        <Tooltip title="Unlike" placement="top">
          <LikeIcon onClick={() => unlikeScream(scream.screamId)} />
        </Tooltip>
      ) : (
        <Tooltip title="like" placement="top">
          <UnlikeIcon onClick={() => likeScream(scream.screamId)} />
        </Tooltip>
      )}
    </IconButton>
  );
  const CommentButton = (
    <IconButton color="primary">
      <CommentIcon />
    </IconButton>
  );
  const DeleteButton =
    user.credentials.handle === scream.userHandle ? (
      <DeleteScream screamId={scream.screamId} />
    ) : null;

  return (
    <Card className={classes.cardContainer}>
      <img
        className={classes.userImage}
        src={scream.userImage}
        alt={`Profile ${scream.userHandle}`}
      />
      <CardContent className={classes.content}>
        <div className={classes.userHandle}>
          <Typography
            component={Link}
            to={`/user/${scream.userHandle}`}
            variant="h5"
            color="primary"
          >
            {scream.userHandle}
          </Typography>
          {DeleteButton}
        </div>
        <Typography variant="body2" color="textSecondary">
          {dayjs(scream.createdAt).fromNow()}
        </Typography>
        <Typography variant="body1">{scream.body}</Typography>
        {LikeButton}
        <span>{scream.likeCount} Likes</span>
        {CommentButton}
        <span>{scream.commentCount} Comments</span>
        <ScreamDialog
          screamId={scream.screamId}
          className={classes.expandIcon}
        />
      </CardContent>
      <div className={classes.controls}></div>
    </Card>
  );
}

const mapActionsToProps = {
  likeScream,
  unlikeScream,
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(
  mapStateToProps,
  mapActionsToProps
)(withStyles(styles)(Scream));
