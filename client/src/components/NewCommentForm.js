import React, { useState } from "react";
import styles from "./styles/NewCommnetFormStyles";

import withStyles from "@material-ui/core/styles/withStyles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import { connect } from "react-redux";
import {createComment} from "../redux/actions/dataActions"

function NewCommentForm({ classes, errors, createComment, screamId, loading }) {
  const [comment, setComment] = useState("");

  const handleChange = e => setComment(e.target.value);
  const handleSubmit = e => {
    e.preventDefault();
    createComment(screamId, comment)
  };
  return (
    <form onSubmit={handleSubmit} className={classes.root}>
      <TextField
      required
        fullWidth
        className={classes.commentTextField}
        error={Boolean(errors.comment)}
        helperText={errors.comment || "Must not be empty"}
        label="Comment"
        value={comment}
        onChange={handleChange}
      />
      <Button variant="contained" color="primary" type="submit" disabled={loading}>
        Comment!
      </Button>
    </form>
  );
}

const mapStateToProps = state => ({
  errors: state.UI.errors,
  loading: state.UI.loading
});

const mapActionsToProps = {
    createComment
}

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(NewCommentForm));
