import React, { useState } from "react";
import { connect } from "react-redux";
import { postScream } from "../redux/actions/dataActions";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

function NewScream({ handleDialog, postScream }) {
  const [scream, setscream] = useState("");
  const [errors, setErrors] = useState({})
  const open = true;
  const handleClose = () => handleDialog(false);
  const handleSubmit = () => {
    if (scream.trim() === ""){
      setErrors({scream: "Must not be empty!"})
    } else{
      postScream({ body: scream });
      handleClose()
    }
  };

  return (
    <Dialog open={open} onClose={handleClose} maxWidth="sm" fullWidth>
      <DialogTitle>New Scream!!</DialogTitle>
      <DialogContent>
        <TextField
          value={scream}
          onChange={e => setscream(e.target.value)}
          error={Boolean(errors.scream)}
          helperText={errors.scream}
          autoFocus
          margin="normal"
          id="scream"
          label="Scream"
          placeholder="Scream your heart out!"
          type="email"
          fullWidth
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleSubmit} color="primary">
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
}

const mapStateToActions = {
  postScream
};
export default connect(null, mapStateToActions)(NewScream);
