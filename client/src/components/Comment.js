import React from "react";
import styles from "./styles/CommentStyles";

import withStyles from "@material-ui/core/styles/withStyles";
import { Typography } from "@material-ui/core";

import dayjs from "dayjs";


function Comment({ comment, classes }) {
  return (
    <div className={classes.commentContainer}>
      <div className={classes.imageContainer}>
        <img
          src={comment.userImage}
          alt={`${comment.userHandle} dp`}
          className={classes.image}
        />
      </div>
      <div className={classes.commentContent}>
        <Typography color="primary" variant="h4">
          {comment.userHandle}
        </Typography>
        <Typography color="textSecondary" variant="body2">
          {dayjs(comment.createdAt).fromNow()}
        </Typography>
        <Typography variant="body2">
          {comment.body}
        </Typography>
      </div>
    </div>
  );
}

export default withStyles(styles)(Comment);
