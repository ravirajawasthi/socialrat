import React, { useState } from "react";
import { connect } from "react-redux";
/* Styles */
import styles from "./styles/NavbarStyles";
// Components
import NewScream from "./NewScream";
import NotificationList from "./NotificationList";

/* MUI */
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles } from "@material-ui/core/styles";

//Icons
import IconButton from "@material-ui/core/IconButton";
import HomeIcon from "@material-ui/icons/Home";
import AddIcon from "@material-ui/icons/Add";

//React Router
import { Link } from "react-router-dom";

function Navbar({ classes, authenticated }) {
  const [screamDialogOpen, setScreamDialogOpen] = useState(false);
  const authenticatedLinks = [
    {
      name: "Post Scream",
      to: "/",
      icon: AddIcon,
      onClick: () => setScreamDialogOpen(true),
    },
    { name: "Home", to: "/", icon: HomeIcon },
  ];
  const navLinks = [
    { name: "Home", to: "/" },
    { name: "Signup", to: "/signup" },
    { name: "login", to: "/login" },
  ];
  return (
    <AppBar color="primary">
      <Toolbar className={classes.navContainer}>
        {authenticated
          ? authenticatedLinks.map((nav, index) => (
              <Tooltip title={nav.name}>
                <IconButton
                  key={index}
                  className={classes.Link}
                  color="primary"
                  onClick={nav.onClick ? nav.onClick : () => {}}
                >
                  <Link key={index} to={nav.to}>
                    <nav.icon />
                  </Link>
                </IconButton>
              </Tooltip>
            ))
          : navLinks.map((nav, index) => (
              <Button key={index} className={classes.Link} color="primary">
                <Link key={index} to={nav.to}>
                  {nav.name}
                </Link>
              </Button>
            ))}
        {authenticated && <NotificationList />}
        {screamDialogOpen && authenticated && (
          <NewScream handleDialog={setScreamDialogOpen} />
        )}
      </Toolbar>
    </AppBar>
  );
}
const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
});
export default connect(mapStateToProps, null)(withStyles(styles)(Navbar));
