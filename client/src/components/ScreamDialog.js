import React, { useState } from "react";
import styles from "./styles/ScreamDialogStyles";
import Comment from "./Comment";
import NewCommnetForm from "./NewCommentForm";

import dayjs from "dayjs";
import {
  getScream,
  likeScream,
  unlikeScream,
  clearErrors,
} from "../redux/actions/dataActions";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Divider from "@material-ui/core/Divider";

import ExpandIcon from "@material-ui/icons/UnfoldMore";
import CircularProgress from "@material-ui/core/CircularProgress";
import LikeIcon from "@material-ui/icons/Favorite";
import UnlikeIcon from "@material-ui/icons/FavoriteBorder";
import CommentIcon from "@material-ui/icons/Comment";

import { connect } from "react-redux";

import { Link } from "react-router-dom";

import withStyles from "@material-ui/core/styles/withStyles";
import { Typography } from "@material-ui/core";

function ScreamDialog({
  screamId,
  scream,
  getScream,
  loading,
  classes,
  user,
  likeScream,
  unlikeScream,
  clearErrors,
}) {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
    getScream(screamId);
  };

  const handleClose = () => {
    setOpen(false);
    clearErrors();
  };
  const hasUserLikedScream = () => {
    if (
      user.likes &&
      user.likes.find((like) => like.screamId === scream.screamId)
    )
      return true;
    else return false;
  };

  const LikeButton = (
    <IconButton color="primary">
      {!user.authenticated ? (
        <Tooltip title="like" placement="top">
          <Link to="/signup">
            <UnlikeIcon />
          </Link>
        </Tooltip>
      ) : hasUserLikedScream() ? (
        <Tooltip title="Unlike" placement="top">
          <LikeIcon onClick={() => unlikeScream(scream.screamId)} />
        </Tooltip>
      ) : (
        <Tooltip title="like" placement="top">
          <UnlikeIcon onClick={() => likeScream(scream.screamId)} />
        </Tooltip>
      )}
    </IconButton>
  );
  const CommentButton = (
    <IconButton color="primary">
      <CommentIcon />
    </IconButton>
  );
  return (
    <>
      <Tooltip title="Expand" placement="top">
        <IconButton onClick={handleClickOpen} color="primary">
          <ExpandIcon />
        </IconButton>
      </Tooltip>
      <Dialog open={open} onClose={handleClose} maxWidth="md" fullWidth>
        {loading ? (
          <CircularProgress className={classes.loadingIcon} />
        ) : (
          <DialogContent>
            <div className={classes.mainContainer}>
              <div className={classes.screamContainer}>
                <div className={classes.imageContainer}>
                  <img
                    src={scream.userImage}
                    alt={`${scream.userHandle} dp`}
                    className={classes.userImage}
                  />
                </div>

                <div className={classes.screamContent}>
                  <Typography color="primary" variant="h4">
                    {scream.userHandle}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    {dayjs(scream.createdAt).fromNow()}
                  </Typography>
                  <Typography variant="body1">{scream.body}</Typography>
                  <div className={classes.screamActions}>
                    <span>
                      {LikeButton}
                      {scream.likeCount} Likes
                    </span>
                    <span>
                      {CommentButton}
                      {scream.commentCount} Comments
                    </span>
                  </div>
                </div>
              </div>
              <Divider />
              <NewCommnetForm screamId={scream.screamId} />
              <Divider />
              {scream.comments &&
                scream.comments.map((comment, index) => (
                  <>
                    <Comment comment={comment} />
                    {index !== scream.comments.length - 1 && <Divider />}
                  </>
                ))}
            </div>
          </DialogContent>
        )}
      </Dialog>
    </>
  );
}

const mapStateToProps = (state) => ({
  scream: state.data.scream,
  loading: state.UI.loading,
  user: state.user,
});

const mapActionToProps = {
  getScream,
  likeScream,
  unlikeScream,
  clearErrors,
};

export default withStyles(styles)(
  connect(mapStateToProps, mapActionToProps)(ScreamDialog)
);
