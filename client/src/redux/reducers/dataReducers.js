import {
  SET_SCREAM,
  SET_SCREAMS,
  LOADING_DATA,
  LIKE_SCREAM,
  UNLIKE_SCREAM,
  DELETE_SCREAM,
  ADD_SCREAM,
  ADD_COMMENT,
  STOP_DATA_LOADING,
  SET_USER_PAGE,
} from "../type";

const initialState = {
  screams: [],
  scream: {},
  user: {},
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOADING_DATA:
      return {
        ...state,
        loading: true,
      };
    case SET_SCREAMS:
      return {
        ...state,
        screams: action.payload,
        loading: false,
      };
    case UNLIKE_SCREAM:
    case LIKE_SCREAM:
      let index = state.screams.findIndex(
        (scream) => scream.screamId === action.payload.screamId
      );
      state.screams[index] = action.payload;
      return {
        ...state,
        scream: action.payload,
      };
    case DELETE_SCREAM: {
      const screamId = action.payload;
      const index = state.screams.findIndex(
        (scream) => scream.screamId === screamId
      );
      state.screams.splice(index, 1);
      return {
        ...state,
      };
    }
    case ADD_SCREAM: {
      return {
        ...state,
        screams: [action.payload, ...state.screams],
      };
    }
    case SET_SCREAM: {
      return { ...state, scream: action.payload };
    }
    case ADD_COMMENT: {
      const createdComment = action.payload;

      //incrementing scream comment count
      state.screams = state.screams.map((scream) => {
        if (scream.screamId === createdComment.screamId) {
          scream.commentCount += 1;
        }
        return scream;
      });
      //adding comment to currently open scream
      state.scream.comments = [action.payload, ...state.scream.comments];
      //incrementing current open scream comment count
      state.scream.commentCount += 1;
      return {
        ...state,
      };
    }
    case STOP_DATA_LOADING: {
      return {
        ...state,
        loading: false,
      };
    }
    case SET_USER_PAGE: {
      return { ...state, user: action.payload };
    }
    default:
      return {
        ...state,
      };
  }
};
