import {
  SET_USER,
  SET_AUTHENTICATED,
  SET_UNAUTHENTICATED,
  LOADING_USER,
  STOP_USER_LOADING,
  LIKE_SCREAM,
  UNLIKE_SCREAM
} from "../type";

const initialState = {
  loading: true,
  authenticated: false,
  credentials: {},
  likes: [],
  notifications: []
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_AUTHENTICATED:
      return {
        ...state,
        loading: false,
        authenticated: true
      };
    case SET_UNAUTHENTICATED:
      return {
        ...initialState
      };
    case SET_USER:
      return {
        ...action.payload,
        loading: false,
        authenticated: true
      };
    case LOADING_USER:
      return {
        ...state,
        loading: true
      };
    case STOP_USER_LOADING:
      return {
        ...state,
        loading: false
      };
    case LIKE_SCREAM:
      return {
        ...state,
        likes: [
          ...state.likes,
          {
            userHandle: state.credentials.handle,
            screamId: action.payload.screamId
          }
        ]
      };
    case UNLIKE_SCREAM:
      return {
        ...state,
        likes: state.likes.filter(
          like => like.screamId !== action.payload.screamId
        )
      };
    default:
      return state;
  }
};
export default userReducer;
