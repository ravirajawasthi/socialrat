import Axios from "axios";

import {
  SET_SCREAMS,
  LOADING_DATA,
  LIKE_SCREAM,
  UNLIKE_SCREAM,
  SET_ERRORS,
  DELETE_SCREAM,
  LOADING_UI,
  CLEAR_ERRORS,
  ADD_SCREAM,
  SET_SCREAM,
  STOP_LOADING_UI,
  ADD_COMMENT,
  STOP_DATA_LOADING,
  SET_USER_PAGE,
} from "../type";

const baseURL =
  !process.env.NODE_ENV || process.env.NODE_ENV === "development"
    ? "http://localhost:5000/socialrat-772eb/us-central1/api"
    : "https://us-central1-socialrat-772eb.cloudfunctions.net/api";

//Get all screams
export const getScreams = () => (dispatch) => {
  dispatch({ type: LOADING_DATA });
  Axios.get(`${baseURL}/screams`)
    .then((res) => {
      dispatch({
        type: SET_SCREAMS,
        payload: res.data,
      });
    })
    .catch((err) => {
      dispatch({ type: SET_SCREAMS, payload: [] });
    });
};

//LIKE A SCREAM
export const likeScream = (screamId) => (dispatch) => {
  Axios.post(`${baseURL}/scream/${screamId}/like`)
    .then((res) => {
      dispatch({ type: LIKE_SCREAM, payload: res.data });
    })
    .catch((err) => dispatch({ type: SET_ERRORS, payload: err.response.data }));
};
//UNLIKE A SCREAM
export const unlikeScream = (screamId) => (dispatch) => {
  Axios.post(`${baseURL}/scream/${screamId}/unlike`)
    .then((res) => {
      dispatch({ type: UNLIKE_SCREAM, payload: res.data });
    })
    .catch((err) => dispatch({ type: SET_ERRORS, payload: err.response.data }));
};

//Delete a scream
export const deleteScream = (screamId) => (dispatch) => {
  Axios.post(`${baseURL}/scream/${screamId}/delete`)
    .then((res) => {
      dispatch({ type: DELETE_SCREAM, payload: screamId });
    })
    .catch((err) => dispatch({ type: SET_ERRORS, payload: err.response.data }));
};

//Post a scream
export const postScream = (body) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  Axios.post(`${baseURL}/scream`, { body: body.body })
    .then((res) => {
      dispatch({ type: ADD_SCREAM, payload: res.data });
      dispatch({ type: CLEAR_ERRORS });
    })
    .catch((err) => {
      dispatch({ type: SET_ERRORS, payload: err.response.data });
    });
};

//Get a particular scream from server
export const getScream = (screamId) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  Axios.get(`${baseURL}/scream/${screamId}`)
    .then((res) => dispatch({ type: SET_SCREAM, payload: res.data }))
    .catch((err) => console.log("Database is not rechable!"))
    .finally(() => dispatch({ type: STOP_LOADING_UI }));
};
//Create a comment
export const createComment = (screamId, commentBody) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  Axios.post(`${baseURL}/scream/${screamId}/comment`, {
    body: commentBody,
  })
    .then((res) => {
      dispatch({ type: ADD_COMMENT, payload: res.data });
    })
    .catch((err) => {
      console.log(err);
      dispatch({ type: SET_ERRORS, payload: err.response.data });
    })
    .finally(() => dispatch({ type: STOP_LOADING_UI }));
};
//Get user profile
export const getUserProfile = (userHandle) => (dispatch) => {
  dispatch({ type: LOADING_DATA });
  Axios.get(`${baseURL}/user/${userHandle}`)
    .then((res) => {
      dispatch({ type: SET_USER_PAGE, payload: res.data });
    })
    .catch((err) => {
      console.log(err);
    })
    .finally(() => dispatch({ type: STOP_DATA_LOADING }));
};

export const clearErrors = () => (dispatch) => dispatch({ type: CLEAR_ERRORS });
