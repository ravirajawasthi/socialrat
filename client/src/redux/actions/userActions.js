import Axios from "axios";
import {
  SET_USER,
  SET_ERRORS,
  CLEAR_ERRORS,
  LOADING_UI,
  SET_UNAUTHENTICATED,
  SET_AUTHENTICATED,
  LOADING_USER,
  STOP_USER_LOADING,
} from "../type";

const baseURL =
  !process.env.NODE_ENV || process.env.NODE_ENV === "development"
    ? "http://localhost:5000/socialrat-772eb/us-central1/api"
    : "https://us-central1-socialrat-772eb.cloudfunctions.net/api";

export const loginUser = (userData, history) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  Axios.post(`${baseURL}/login`, userData)
    .then((res) => {
      localStorage.setItem("FbIdToken", res.data.token);
      setAuthrizationToken(res.data.token);
      dispatch(getUserData());
      dispatch({ type: CLEAR_ERRORS });
      history.push("/");
    })
    .catch((err) => {
      dispatch({ type: SET_ERRORS, payload: err.response.data });
    });
};

export const getUserData = () => (dispatch) => {
  dispatch({ type: LOADING_USER });
  Axios.get(`${baseURL}/user`)
    .then((res) => {
      dispatch({ type: SET_USER, payload: res.data });
      dispatch({ type: SET_AUTHENTICATED });
    })
    .catch((err) => {
      console.log(err);
    });
};

export const signUpUser = (newUserData, history) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  Axios.post(`${baseURL}/signup`, newUserData)
    .then((res) => {
      setAuthrizationToken(res.data.token);
      dispatch(getUserData());
      dispatch({ type: CLEAR_ERRORS });
      history.push("/");
    })
    .catch((err) => {
      dispatch({ type: SET_ERRORS, payload: err.response.data });
    });
};

export const logoutUser = () => (dispatch) => {
  dispatch({ type: SET_UNAUTHENTICATED });
  delete localStorage["FbIdToken"];
  delete Axios.defaults.headers.common["Authotization"];
  //The loading icon seems kind of cool
  setTimeout(() => dispatch({ type: STOP_USER_LOADING }), 1000);
};

export const uploadImage = (formData) => (dispatch) => {
  dispatch({ type: LOADING_USER });
  Axios.post(`${baseURL}/user/image`, formData)
    .then(() => {
      dispatch(getUserData());
    })
    .catch((err) => {
      dispatch({ type: SET_ERRORS, payload: err.response.data });
    });
};

export const updateUserInfo = (details) => (dispatch) => {
  dispatch({ type: LOADING_USER });
  Axios.post(`${baseURL}/user`, details)
    .then((res) => {
      dispatch(getUserData());
    })
    .catch((err) => {
      dispatch({ type: SET_ERRORS, payload: err.response.data });
    });
};

export const clearErrors = () => (dispatch) => {
  dispatch({ type: CLEAR_ERRORS });
};

const setAuthrizationToken = (token) => {
  localStorage.setItem("FbIdToken", token);
  Axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
};
