//user reducer types
export const SET_AUTHENTICATED = "SET_AUTHENTICATED";
export const SET_UNAUTHENTICATED = "SET_UNAUTHENTICATED";
export const SET_USER = "SET_USER";
export const LOADING_USER = "LOADING_USER";
export const STOP_USER_LOADING = "STOP_USER_LOADING";


//ui reducer types
export const SET_ERRORS = "SET_ERRORS";
export const LOADING_UI = "LOADING_UI";
export const CLEAR_ERRORS = "CLEAR_ERRORS"
export const LOADING_DATA = "LOADING_DATA"
export const  STOP_LOADING_UI = "STOP_LOADING_UI"

//data reducer types
export const SET_SCREAM = "SET_SCREAM"
export const SET_SCREAMS = "SET_SCREAMS"
export const LIKE_SCREAM = "LIKE_SCREAM"
export const UNLIKE_SCREAM = "UNLIKE_SCREAM"
export const DELETE_SCREAM = "DELETE_SCREAM"
export const ADD_SCREAM = "ADD_SCREAM"
export const ADD_COMMENT = "ADD_COMMENT"
export const STOP_DATA_LOADING = "STOP_DATA_LOADING"
export const SET_USER_PAGE = "SET_USER_PAGE"