import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from 'redux-thunk';

import userReducer from "./reducers/userReducer";
import dataReducer from "./reducers/dataReducers";
import uiReducer from "./reducers/uiReducer";

const initialState = {};

const middleWear = [thunk];

const reducers = combineReducers({
  user: userReducer,
  data: dataReducer,
  UI: uiReducer
});

const store = createStore(
  reducers,
  initialState,
  composeWithDevTools(
    applyMiddleware(...middleWear),
  )
);


export default store;