import React, { useEffect } from "react";
import styles from "./styles/HomeStyles";
//Components
import Scream from "../components/Scream";
import Profile from "../components/Profile";
//MUI Stuff
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import withStyles from "@material-ui/core/styles/withStyles";
//Redux
import { getScreams } from "../redux/actions/dataActions";
import { connect } from "react-redux";

function Home({ data: { screams, loading }, getScreams, classes }) {
  useEffect(() => {
    getScreams();
  }, [getScreams]);
  const screamsMarkup =
    screams.length === 0 ? (
      <p className={classes.noScreamsFound}>No Screams Found</p>
    ) : (
      screams.map((scream) => <Scream scream={scream} key={scream.screamId} />)
    );
  return (
    <Grid container spacing={2}>
      <Grid item sm={8} xs={12}>
        {!loading ? (
          screamsMarkup
        ) : (
          <div className={classes.loadingProgress}>
            <CircularProgress />
            <p>Loading...</p>
          </div>
        )}
      </Grid>
      <Grid item sm={4} xs={12}>
        <Profile />
      </Grid>
    </Grid>
  );
}

const mapStateToProps = (state) => ({
  data: state.data,
});

export default withStyles(styles)(
  connect(mapStateToProps, { getScreams })(Home)
);
