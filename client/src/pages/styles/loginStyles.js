const styles = {
  container: {
    textAlign: "center",
    "& img": {
      marginTop: "20px",
      maxHeight: "100px",
    },
  },
  title: {
    marginTop: "10px",
  },
  textField: {
    margin: "5px auto",
  },
  submitBtn: {
    marginTop: "20px",
    maxHeight: "50px",
  },
  errorMessage: {
    color: "red",
    marginTop: "20px",
  },
  signUpMessage: {
    display: "block",
    marginTop: "10px",
  },

};

export default styles;
