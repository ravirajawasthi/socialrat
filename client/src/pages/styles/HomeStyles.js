const styles = (theme) => ({
  loadingProgress: {
    width: "100%",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  noScreamsFound: {
    textAlign: "center",
    color: "gray",
  },
});

export default styles;
