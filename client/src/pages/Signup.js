//Components and react
import React, { useEffect, useState } from "react";
import styles from "./styles/loginStyles";
import Logo from "./images/ratLogo.png";
import { Link as LinkRouter } from "react-router-dom";

//Material Ui
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import CircularProgress from "@material-ui/core/CircularProgress";
import Link from "@material-ui/core/Link";

//Redux imports
import { connect } from "react-redux";
import { signUpUser } from "../redux/actions/userActions";
import { clearErrors } from "../redux/actions/dataActions";

function Signup({
  classes,
  signUpUser,
  clearErrors,
  UI: { loading, errors },
  history,
}) {
  useEffect(() => {
    clearErrors();
  }, [clearErrors]);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [handle, setHandle] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    const signUpDetails = {
      email,
      password,
      confirmPassword,
      handle,
    };
    signUpUser(signUpDetails, history);
  };
  return (
    <Grid container className={classes.container}>
      <Grid item sm xs={false} />
      <Grid item sm xs={12}>
        <img src={Logo} alt="Login Logo" />
        <Typography variant="h2" className={classes.title}>
          Sign Up
        </Typography>
        <form noValidate onSubmit={handleSubmit}>
          <TextField
            className={classes.textField}
            name="email"
            type="email"
            id="email"
            helperText={errors.email}
            error={Boolean(errors.email)}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            fullWidth
            label="Email"
          />
          <TextField
            className={classes.textField}
            name="password"
            type="password"
            id="password"
            helperText={errors.password}
            error={Boolean(errors.password)}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            fullWidth
            label="Password"
          />
          <TextField
            className={classes.textField}
            name="confirmPassword"
            type="password"
            id="confirmPassword"
            helperText={errors.confirmPassword}
            error={Boolean(errors.confirmPassword)}
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
            fullWidth
            label="Confirm Password"
          />
          <TextField
            className={classes.textField}
            name="handle"
            type="text"
            id="handle"
            helperText={errors.handle}
            error={Boolean(errors.handle)}
            value={handle}
            onChange={(e) => setHandle(e.target.value)}
            fullWidth
            label="Handle"
          />
          <Button
            className={classes.submitBtn}
            type="submit"
            variant="contained"
            color="primary"
            disabled={loading}
          >
            {loading ? <CircularProgress /> : "Create Account"}
          </Button>
          {errors.general && (
            <Typography className={classes.errorMessage} variant="body2">
              {errors.general}
            </Typography>
          )}
        </form>
        <small className={classes.signUpMessage}>
          Already have an account? login{" "}
          <Link>
            <LinkRouter to="/login">here</LinkRouter>
          </Link>
        </small>
      </Grid>
      <Grid item sm xs={false} />
    </Grid>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
  UI: state.UI,
});

const mapActionsToProps = {
  signUpUser,
  clearErrors,
};

export default withStyles(styles)(
  connect(mapStateToProps, mapActionsToProps)(Signup)
);
