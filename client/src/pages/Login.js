import React, { useState, useEffect } from "react";
import styles from "./styles/loginStyles";
import Logo from "./images/ratLogo.png";
import { Link as LinkRouter } from "react-router-dom";

import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import withStyles from "@material-ui/core/styles/withStyles";
import CircularProgress from "@material-ui/core/CircularProgress";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

import { connect } from "react-redux";
import { loginUser } from "../redux/actions/userActions";
import { clearErrors } from "../redux/actions/dataActions";

function Login({
  classes,
  history,
  loginUser,
  UI: { errors, loading },
  clearErrors,
}) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    clearErrors();
  }, [clearErrors]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const loginDetails = {
      email,
      password,
    };
    loginUser(loginDetails, history); //history is coming from react-router-dom, it is used for redirection
  };
  return (
    <Grid container className={classes.container}>
      <Grid item sm xs={false} />
      <Grid item sm xs={12}>
        <img src={Logo} alt="Login Logo" />
        <Typography variant="h2" className={classes.title}>
          Login
        </Typography>
        <form noValidate onSubmit={handleSubmit}>
          <TextField
            className={classes.textField}
            name="email"
            type="email"
            id="email"
            helperText={errors.email}
            error={Boolean(errors.email)}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            fullWidth
            label="Email"
          />
          <TextField
            className={classes.textField}
            name="password"
            type="password"
            id="password"
            helperText={errors.password}
            error={Boolean(errors.password)}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            fullWidth
            label="Password"
          />
          <Button
            className={classes.submitBtn}
            type="submit"
            variant="contained"
            color="primary"
            disabled={loading}
          >
            {loading ? <CircularProgress /> : "Login"}
          </Button>
          <small className={classes.signUpMessage}>
            Don't have an account? sign-up{" "}
            <Link>
              <LinkRouter to="/signup">here</LinkRouter>
            </Link>
          </small>
          {errors.general && (
            <Typography className={classes.errorMessage} variant="body2">
              {errors.general}
            </Typography>
          )}
        </form>
      </Grid>
      <Grid item sm xs={false} />
    </Grid>
  );
}

const mapStateToProps = (state) => ({
  user: state.user,
  UI: state.UI,
});

const mapActionsToProps = {
  loginUser,
  clearErrors,
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(withStyles(styles)(Login));
