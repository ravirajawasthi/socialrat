import React, { useState, useEffect } from "react";
import styles from "./styles/UserStyles";

import Scream from "../components/Scream";
import StaticProfile from "../components/StaticProfile.js";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import CircularProgress from "@material-ui/core/CircularProgress";
import withStyles from "@material-ui/core/styles/withStyles";

import { getUserProfile } from "../redux/actions/dataActions";
import { connect } from "react-redux";

function Home({
  data: {
    user: { screams, user },
    loading,
  },
  getUserProfile,
  classes,
  match,
}) {
  const [firstLoad, setFirstLoad] = useState(true);

  useEffect(() => {
    getUserProfile(match.params.userHandle);
    setFirstLoad(false);
  }, [firstLoad, getUserProfile, match.params.userHandle]);
  const screamsMarkup = screams ? (
    screams.map((scream) => <Scream scream={scream} key={scream.screamId} />)
  ) : (
    <p>Loading</p>
  );
  return (
    <Grid container spacing={2}>
      <Grid item sm={8} xs={12}>
        {!loading ? (
          screamsMarkup
        ) : (
          <div className={classes.loadingProgress}>
            <CircularProgress />
            <p>Loading...</p>
          </div>
        )}
      </Grid>
      <Grid item sm={4} xs={12}>
        {user ? (
          <StaticProfile />
        ) : (
          <>
            <Paper className={classes.loadingProgress}>
              <CircularProgress />
              <p>Loading...</p>
            </Paper>
          </>
        )}
      </Grid>
    </Grid>
  );
}

const mapStateToProps = (state) => ({
  data: state.data,
});

export default withStyles(styles)(
  connect(mapStateToProps, { getUserProfile })(Home)
);
